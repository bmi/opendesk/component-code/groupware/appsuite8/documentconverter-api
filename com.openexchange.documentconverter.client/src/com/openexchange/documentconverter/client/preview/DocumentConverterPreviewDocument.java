/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.client.preview;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.openexchange.preview.RemoteInternalPreviewDocument;

/**
 * {@link DocumentConverterPreviewDocument}
 *
 * @author <a href="mailto:oliver.specht@open-xchange.com">Oliver Specht</a>
 */
class DocumentConverterPreviewDocument implements RemoteInternalPreviewDocument{

    private final Map<String, String> m_metaData = new HashMap<>();
    private final List<String> m_content;
    private final byte[] m_previewBuffer;
    private final Boolean m_moreAvailable;

    public DocumentConverterPreviewDocument(final Map<String, String> metaData, final List<String> content, byte[] previewBuffer, Boolean moreAvailable) {
        super();

        m_metaData.putAll(metaData);
        m_content = content;
        m_previewBuffer = previewBuffer;
        m_moreAvailable = moreAvailable;
    }
    @Override
    public Map<String, String> getMetaData() {
        return m_metaData;
    }

    @Override
    public boolean hasContent() {
        return null != m_content;
    }

    @Override
    public List<String> getContent() {
        return m_content;
    }

    @Override
    public InputStream getThumbnail() {
        return ((null != m_previewBuffer) ? new ByteArrayInputStream(m_previewBuffer) : null);
    }

    @Override
    public Boolean isMoreAvailable() {
        return m_moreAvailable;
    }

    @Override
    public String toString() {
        return ((null == m_content) ? super.toString() : m_content.toString());
    }

    @Override
    public byte[] getThumbnailBuffer() {
        return m_previewBuffer;
    }

}

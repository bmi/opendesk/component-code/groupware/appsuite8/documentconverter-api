/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.client.json;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import com.openexchange.ajax.container.FileHolder;
import com.openexchange.ajax.fileholder.IFileHolder;
import com.openexchange.ajax.requesthandler.AJAXRequestData;
import com.openexchange.ajax.requesthandler.AJAXRequestResult;
import com.openexchange.documentconverter.DocumentConverterManager;
import com.openexchange.documentconverter.FileAndURL;
import com.openexchange.documentconverter.FileIdManager;
import com.openexchange.documentconverter.IDocumentConverter;
import com.openexchange.documentconverter.JobErrorEx;
import com.openexchange.documentconverter.JobPriority;
import com.openexchange.documentconverter.MutableWrapper;
import com.openexchange.documentconverter.NonNull;
import com.openexchange.documentconverter.Nullable;
import com.openexchange.documentconverter.Properties;
import com.openexchange.documentconverter.client.impl.ClientConfig;
import com.openexchange.documentconverter.client.impl.ClientManager;
import com.openexchange.file.storage.DefaultFile;
import com.openexchange.file.storage.File.Field;
import com.openexchange.file.storage.FileStorageFileAccess;
import com.openexchange.file.storage.FileStorageFolder;
import com.openexchange.file.storage.FileStoragePermission;
import com.openexchange.file.storage.composition.IDBasedFileAccess;
import com.openexchange.file.storage.composition.IDBasedFileAccessFactory;
import com.openexchange.file.storage.composition.IDBasedFolderAccess;
import com.openexchange.file.storage.composition.IDBasedFolderAccessFactory;
import com.openexchange.java.Streams;
import com.openexchange.server.ServiceLookup;
import com.openexchange.tools.session.ServerSession;

/**
 * {@link GetDocumentAction}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 */
public class GetDocumentAction extends DocumentConverterAction {

    final private static int ERRORCODE_NONE = 0;
    final private static int ERRORCODE_BAD_REQUEST = 400;
    final private static char[] INVALID_FILENAME_CHARS = { '/', '\\', ':', '*', '?', '"', '|', '<', '>' };
    final private static String ERRORTEXT_INVALID_FILENAME = "invalidFilename";

    /**
     * Initializes a new {@link GetDocumentAction}.
     *
     * @param services
     */
    public GetDocumentAction(final ServiceLookup services, final ClientManager manager, final FileIdManager fileIdManager, ClientConfig clientConfig) {
        super(services, manager, fileIdManager, clientConfig);
    }

    /* (non-Javadoc)
     * @see com.openexchange.documentconverter.json.actions.DocumentConverterAction#perform(com.openexchange.ajax.requesthandler.AJAXRequestData, com.openexchange.tools.session.ServerSession)
     */
    @Override
    public AJAXRequestResult perform(@NonNull AJAXRequestData request, @NonNull ServerSession session) {
        String fileName = request.getParameter("filename");
        String mimeType = request.getParameter("mimetype");
        final String fileExtension = FilenameUtils.getExtension((null != fileName) ? fileName : "").trim().toLowerCase();
        final MutableWrapper<Boolean> documentDecrypted = new MutableWrapper<>(Boolean.FALSE);
        final MutableWrapper<String> jobId = new MutableWrapper<>(null);
        final MutableWrapper<JobErrorEx> jobErrorExWrapper = new MutableWrapper<>(new JobErrorEx());
        FileAndURL resultFileAndURL = null;
        final boolean isAsync = "true".equalsIgnoreCase(request.getParameter("async"));

        // don't convert to PDF, if it is requested ("documentformat==pdf") but the source file is a PDF file
        // already, determined by either the file extension ("pdf") or the mime type ("application/pdf")
        final boolean convertToPDF =
            "pdf".equalsIgnoreCase(request.getParameter("documentformat")) &&
            !"pdf".equals(fileExtension) && !"application/pdf".equalsIgnoreCase(mimeType);

        if ((!convertToPDF ||
            ((null == (resultFileAndURL = getPDFFileAndURL(request, session, isAsync, documentDecrypted, jobId, jobErrorExWrapper))) &&
             jobErrorExWrapper.get().hasNoError())) && !isAsync) {

            final IDocumentConverter docConverter = m_services.getService(IDocumentConverter.class);
            final String encTest = request.getParameter("enctest");

            // In case of PDF input and output, the result file is equal to the source file
            resultFileAndURL = getRequestSourceFileAndURL(request, session, documentDecrypted, jobErrorExWrapper);

            // If source/result file is a PDF file and encTest is set to 'pdf', do an
            // encryption test to get a valid error code in case of an encrypted PDF
            if (!convertToPDF &&
                (null != resultFileAndURL) &&
                (null != encTest) && encTest.equalsIgnoreCase("pdf") &&
                (null != docConverter)) {

                final HashMap<String, Object> jobProperties = new HashMap<>(12);
                final HashMap<String, Object> resultProperties = new HashMap<>(8);

                jobProperties.put(Properties.PROP_INPUT_FILE, resultFileAndURL.getFile());
                jobProperties.put(Properties.PROP_PRIORITY, JobPriority.fromString(request.getParameter("priority")));
                jobProperties.put(Properties.PROP_LOCALE, getPreferredLanguage(session));

                final String inputURL = resultFileAndURL.getURL();

                if (null != inputURL) {
                    jobProperties.put(Properties.PROP_INPUT_URL, inputURL);
                }

                if (null != fileName) {
                    jobProperties.put(Properties.PROP_INFO_FILENAME, fileName);
                }

                // do the PDF encryption test and close possible result stream immediately
                Streams.close(docConverter.convert("pdfenctest", jobProperties, resultProperties));

                jobErrorExWrapper.set(isAsync ? new JobErrorEx() : JobErrorEx.fromResultProperties(resultProperties));
            }
        }

        // create appropriate response result
        final String resultFileId = jobId.get();
        IFileHolder resultFileHolder = null;
        JSONObject jsonResult = null;

        try {
            if (null != resultFileId) {
                m_fileIdManager.lock(resultFileId);
            }

            if (isAsync) {
                (jsonResult = new JSONObject()).put("async", true);
            } else if (null != resultFileAndURL) {
                final File resultFile = resultFileAndURL.getFile();
                final String sessionId = request.getParameter("session");
                final String saveAsFileName = request.getParameter("saveas_filename");
                final String saveAsFolderId = request.getParameter("saveas_folder_id");

                if (StringUtils.isBlank(fileName) || !"pdf".equalsIgnoreCase(FilenameUtils.getExtension(fileName))) {
                    final StringBuilder fileNameBuilder = new StringBuilder(StringUtils.isBlank(fileName) ? FORMAT_FILE : fileName);
                    final int curExtPos = fileNameBuilder.lastIndexOf(".");

                    // set or replace extension with ".pdf"
                    mimeType = "application/pdf";
                    fileName = ((curExtPos > -1) ?
                        fileNameBuilder.replace(curExtPos, fileNameBuilder.length(),".pdf") :
                            fileNameBuilder.append(".pdf")).toString();
                }

                jsonResult = getJSONResultForSaveAs(session, sessionId, resultFile, saveAsFileName, saveAsFolderId, mimeType);

                if (null == jsonResult) {
                    resultFileHolder = new FileHolder(FileHolder.newClosureFor(resultFile), resultFile.length(), mimeType, fileName);
                } else if (jsonResult.getInt(JSON_KEY_ERRORCODE) > ERRORCODE_NONE) {
                    return getRequestResult(request, null, jsonResult, jobErrorExWrapper.get());
                }
            }
        } catch (Exception e) {
            DocumentConverterManager.logExcp(e);
        } finally {
            if (null != resultFileId) {
                m_fileIdManager.unlock(resultFileId);
            }
        }

        return getRequestResult(request, resultFileHolder, jsonResult, jobErrorExWrapper.get());
    }

    /**
     * @param session
     * @param resultFile
     * @param saveAsFileName
     * @param saveAsFolderId
     * @param givenFileName
     * @param givenMimeType
     * @return
     */
    @Nullable protected JSONObject getJSONResultForSaveAs(
        @NonNull ServerSession session,
        String sessionId,
        File resultFile,
        String saveAsFileName,
        String saveAsFolderId,
        String givenMimeType) throws Exception {

        JSONObject ret = null;

        if ((null != resultFile) && resultFile.canRead() && (resultFile.length() > 0) && !StringUtils.isEmpty(saveAsFileName) && !StringUtils.isEmpty(saveAsFolderId)) {
            if ((null == sessionId) || !sessionId.equals(session.getSessionID())) {
                // #60025: we need to check for an illegal CSRF attempt by checking the correct sessionId;
                // set errorcode to 400 in case of an invalid request
                DocumentConverterManager.logError("DC client Ajax handler was called with invalid sessionId for save request: " + sessionId);

                ret = (new JSONObject()).put(JSON_KEY_ERRORCODE, ERRORCODE_BAD_REQUEST);
            } else if (StringUtils.containsAny(saveAsFileName, INVALID_FILENAME_CHARS)) {
                // #6xxx: check the given 'saveas_filename' parameter for not containing any invalid characters;
                // set errorcode to 400 in case of an invalid 'saveas_filename' parameter
                DocumentConverterManager.logError("DC client Ajax handler was called with an invalid 'saveas_filename' parameter for save request: " + sessionId);

                final JSONObject jsonError = new JSONObject();
                final List<String> invalidCharsList = new ArrayList<>();

                for (final char curInvalidChar : INVALID_FILENAME_CHARS) {
                    if (StringUtils.contains(saveAsFileName, curInvalidChar)) {
                        invalidCharsList.add(String.valueOf(curInvalidChar));
                    }
                }

                jsonError.put(JSON_KEY_ERRORCODE, ERRORCODE_BAD_REQUEST);
                jsonError.put(JSON_KEY_ERRORTEXT, ERRORTEXT_INVALID_FILENAME);
                jsonError.put(JSON_KEY_INVALID_CHARS, new JSONArray(invalidCharsList));

                ret = jsonError;
            } else {
                final IDBasedFolderAccessFactory folderFactory = m_services.getService(IDBasedFolderAccessFactory.class);
                final IDBasedFolderAccess folderAccess = (null != folderFactory) ? folderFactory.createAccess(session) : null;

                if (null != folderAccess) {
                    final FileStorageFolder folder = folderAccess.getFolder(saveAsFolderId);
                    final FileStoragePermission permission = (null != folder) ? folder.getOwnPermission() : null;

                    if ((null != permission) && (permission.getFolderPermission() >= FileStoragePermission.CREATE_OBJECTS_IN_FOLDER)) {
                        final IDBasedFileAccessFactory fileFactory = m_services.getService(IDBasedFileAccessFactory.class);
                        final IDBasedFileAccess fileAccess = (null != fileFactory) ? fileFactory.createAccess(session) : null;

                        if (null != fileAccess) {
                            final com.openexchange.file.storage.File saveAsFileAccessFile = new DefaultFile();

                            saveAsFileAccessFile.setId(FileStorageFileAccess.NEW);
                            saveAsFileAccessFile.setFolderId(saveAsFolderId);
                            saveAsFileAccessFile.setFileName(saveAsFileName);
                            saveAsFileAccessFile.setFileMIMEType(givenMimeType);

                            try (final InputStream resultFileInputStm = FileUtils.openInputStream(resultFile)) {
                                String resultFileId = fileAccess.saveDocument(saveAsFileAccessFile, resultFileInputStm, FileStorageFileAccess.DISTANT_FUTURE, new ArrayList<Field>());

                                if (!StringUtils.isEmpty(resultFileId)) {
                                    (ret = new JSONObject()).
                                    put("id", resultFileId).
                                    put("filename", saveAsFileAccessFile.getFileName()).
                                    put(JSON_KEY_ERRORCODE, 0);
                                }
                            }

                            fileAccess.commit();
                        }
                    }
                }
            }
        }

        return ret;
    }
}

/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter;

import org.apache.commons.lang3.StringUtils;

/**
 * {@link JobPriority}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 */
public enum JobPriority {

    BACKGROUND,
    LOW,
    MEDIUM,
    HIGH,
    INSTANT;

    /**
     * @return the lowest of all possible priorities
     */
    static public JobPriority lowest() {
        return JobPriority.BACKGROUND;
    }

    /**
     * @return the highest of all possible priorities
     */
    static public JobPriority highest() {
        return JobPriority.INSTANT;
    }

    /**
     * @param jobPriorityString
     * @return
     */
    static public JobPriority fromString(final String jobPriorityString) {
        final String testString = StringUtils.lowerCase(jobPriorityString);

        if (null != testString) {
            switch (testString) {
                case ("instant"): return JobPriority.INSTANT;
                case ("high"): return JobPriority.HIGH;
                case ("medium"): return JobPriority.MEDIUM;
                case ("low"): return JobPriority.LOW;
                case ("background"):
                default:
                    return JobPriority.BACKGROUND;
            }
        }

        return JobPriority.BACKGROUND;
    }
}

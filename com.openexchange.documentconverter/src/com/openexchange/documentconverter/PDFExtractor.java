/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.base.Throwables;
import com.openexchange.exception.ExceptionUtils;

/**
 * {@link PDFExtractor}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v7.10.1
 */
public class PDFExtractor {

    final public static int PDFTOOL_ERRORCODE_TIMEOUT = -1;

    final public static int PDFTOOL_ERRORCODE_NONE = 0x00000000;

    final public static int PDFTOOL_ERRORCODE_GENERAL = 0x0000001;

    final public static int PDFTOOL_ERRORCODE_PASSWORD = 0x00000010;

    final private static String PDF_EXTRACTOR_LOG_PREFIX = "DC PDFExtractor";

    /**
     * Unused
     */
    @SuppressWarnings("unused")
    private PDFExtractor() {
        // Unused
        m_manager = null;
        m_pdfExecutable = null;
        m_bashCommandLine = null;
        m_maxExecTimeMillis = 0;
    }

    /**
     * Initializes a new {@link PDFExtractor}.
     * @param manager
     * @param pdfExecutable
     * @param maxVMemMB
     * @param maxExecTimeMillis
     * @throws IOException
     */
    public PDFExtractor(@NonNull final DocumentConverterManager manager,
        @NonNull final File pdfExecutable,
        final long maxVMemMB,
        final long maxExecTimeMillis) throws IOException {

        super();

        if (!pdfExecutable.canExecute()) {
            throw new IOException(implGetLogBuilder("external tool cannot be found or executed: ").
                append(pdfExecutable.getAbsolutePath()).toString());
        } else if (LOG.isTraceEnabled()) {
            LOG.trace(implGetLogBuilder("external tool found: ").
                append(pdfExecutable.getAbsolutePath()).toString());
        }

        m_manager = manager;
        m_pdfExecutable = pdfExecutable;
        m_bashCommandLine = DocumentConverterManager.getMaxVMemBashCommandLine(maxVMemMB);
        m_maxExecTimeMillis = maxExecTimeMillis;
    }

    /**
     * Initializes a new {@link PDFExtractor} with a default timeout of 60000 milliseconds.
     * @param manager
     * @param pdfExecutable
     * @param maxVMemMB
     * @throws IOException
     */
    public PDFExtractor(@NonNull final DocumentConverterManager manager,
        @NonNull final File pdfExecutable,
        final long maxVMemMB) throws IOException {

       this(manager, pdfExecutable, maxVMemMB, 60000);
    }

    /**
     * getLogPrefix
     *
     * @return
     */
    public String getLogPrefix() {
        return PDF_EXTRACTOR_LOG_PREFIX;
    }

    /**
     * @param pdfInputFile
     * @return
     */
    public int getPageCount(@NonNull final File pdfInputFile, @Nullable String fileName) {
        final File pdfWorkDir = m_manager.createTempDir("oxdccpdfpc");
        final File pdfPageCountOutputFile = new File(pdfWorkDir, "pagecount.json");
        final String[] commandLineParts = {
            m_pdfExecutable.getAbsolutePath(),
            "-pagecount",
            pdfInputFile.getAbsolutePath(),
            pdfPageCountOutputFile.getAbsolutePath()
        };

        int ret = -1;

        try {
            final Process pdfToolProcess = Runtime.getRuntime().exec(DocumentConverterManager.getCommandLine(m_bashCommandLine, commandLineParts));
            final int exitValue = DocumentConverterManager.waitForProcess(pdfToolProcess, m_maxExecTimeMillis);

            if (PDFTOOL_ERRORCODE_NONE == exitValue) {
                try (final InputStream inputStm = new FileInputStream(pdfPageCountOutputFile)) {
                    ret = new JSONObject(IOUtils.toString(inputStm, "UTF-8")).getInt("pageCount");
                }

                if (DocumentConverterManager.isLogTrace()) {
                    DocumentConverterManager.logTrace(
                        implGetLogBuilder("detected " + ret + " page(s) in document").toString(),
                        new LogData("filename", (null != fileName) ? fileName : DocumentConverterUtil.STR_UNKNOWN));
                }
            } else {
                // kill process ultimately in case of a timeout error to get rid of looping processes
                if ((PDFTOOL_ERRORCODE_TIMEOUT == ret) && (null != pdfToolProcess)) {
                    pdfToolProcess.destroyForcibly();
                }

                implHandlePDFToolError(exitValue, fileName);
            }
        } catch (Throwable e) {
            ExceptionUtils.handleThrowable(e);
            DocumentConverterManager.logTrace(
                implGetLogBuilder("caught exception when getting page count of PDF file: ").append(Throwables.getRootCause(e).getMessage()).toString(),
                new LogData("filename", (null != fileName) ? fileName : DocumentConverterUtil.STR_UNKNOWN));

            ret = -1;
        } finally {
            FileUtils.deleteQuietly(pdfWorkDir);
        }

        return ret;
    }

    /**
     * Extracting and converting one single page or all pages of the PDF input file into
     * the requested graphics target format, which can be either JPEG or PNG.
     *
     * @param pdfInputFile The PDF file from which single or all pages are  to be extracted
     * @param outputFile The target file, being either a zip archive, if all pages are to
     *  be extracted or the file, containing the extracted page in the requested target format
     * @param targetFormatShortName Either 'jpg' for JPEG target format or 'png' for PNG target format
     * @param pageNumber Use 0 or -1, if all pages are to be extracted into a zip archive,
     *  with each page entry within the zip file having the requested graphics target format.
     *  Values >0 represent a single page, that is to be extracted into the requested
     * @return The numeric errorCode of the extraction, with 0 indicating success.
     */
    public int extractToGraphic(
        @NonNull final File pdfInputFile,
        @NonNull final File outputFile,
        @NonNull final String targetFormatShortName,
        final int pageNumber,
        final int pixelWidth,
        final int pixelHeight,
        @Nullable String scaleType,
        @Nullable String fileName) throws Exception {

        int ret = PDFTOOL_ERRORCODE_GENERAL;

        final String pages = (pageNumber > 0) ? Integer.toString(pageNumber) : "all";
        final String width = (pixelWidth > 0) ? Integer.toString(pixelWidth) : "-1";
        final String height = (pixelHeight > 0) ? Integer.toString(pixelHeight) : "-1";
        final String scaling ="cover".equalsIgnoreCase(scaleType) ? "cover" : "contain";
        final File pdfWorkDir = m_manager.createTempDir("oxdccpdf2g");
        final String resultExtensionSwitch = "png".equalsIgnoreCase(targetFormatShortName) ?
            "-png" : ("svg".equalsIgnoreCase(targetFormatShortName) ?
                "-svg" :
                    "-jpg");

        try {
            final String[] commandLine = {
                m_pdfExecutable.getAbsolutePath(),
                resultExtensionSwitch,
                width,
                height,
                scaling,
                pdfInputFile.getAbsolutePath(),
                new File(pdfWorkDir, "%d#%d_%dx%d").getAbsolutePath(),
                pages
            };

            final Process pdfToolProcess = Runtime.getRuntime().exec(DocumentConverterManager.getCommandLine(m_bashCommandLine, commandLine));

            ret = DocumentConverterManager.waitForProcess(pdfToolProcess, m_maxExecTimeMillis);

            if (PDFTOOL_ERRORCODE_NONE == ret) {
                implPostProcess(pdfWorkDir, outputFile);

                if (DocumentConverterManager.isLogTrace()) {
                    DocumentConverterManager.logTrace(
                        implGetLogBuilder("extracted page '" + pages + "' with format: " + resultExtensionSwitch.substring(1)).toString(),
                        new LogData("filename", (null != fileName) ? fileName : DocumentConverterUtil.STR_UNKNOWN));

                 }
           } else {
               // kill process ultimately in case of a timeout error to get rid of looping processes
               if ((PDFTOOL_ERRORCODE_TIMEOUT == ret) && (null != pdfToolProcess)) {
                   pdfToolProcess.destroyForcibly();
               }

               implHandlePDFToolError(ret, fileName);
            }
        } catch (Throwable e) {
            ExceptionUtils.handleThrowable(e);
            DocumentConverterManager.logError(
                implGetLogBuilder("caught exception when extracting PDF page as graphic: ").append(Throwables.getRootCause(e).getMessage()).toString(),
                new LogData("filename", (null != fileName) ? fileName : DocumentConverterUtil.STR_UNKNOWN));

            ret = PDFTOOL_ERRORCODE_GENERAL;
        } finally {
            FileUtils.deleteQuietly(pdfWorkDir);
        }

        return ret;
    }

    // - Implementation --------------------------------------------------------

    /**
     * implGetLogBuilder
     *
     * @param logText
     * @return
     */
    private StringBuilder implGetLogBuilder(@Nullable final String logText) {
        return new StringBuilder(256).
            append(getLogPrefix()).
            append(' ').append((null != logText) ? logText : "");

    }

    /**
     * @param pdfToolExecCode
     */
    private void implHandlePDFToolError(final int pdfToolExecCode, @Nullable final String fileName) {
        if (0 != pdfToolExecCode) {
            String cause = "general";
            String explain = null;

            switch (pdfToolExecCode) {
                case (PDFTOOL_ERRORCODE_TIMEOUT): cause = "timeout"; break;
                case (0x0002): cause = "argument count"; break;
                case (0x0004): cause = "page count"; break;
                case (0x0008): cause = "page conversion"; break;
                case (0x0010): cause = "encryption"; break;
                case (0x007f): {
                    cause = "out of memory";
                    explain = "please see 'com.openexchange.documentconverter.client.pdfextractor.maxVMemMB' configuration item";
                    break;
                }
                default: {
                    break;
                }
            }

            final StringBuilder logBuilder = implGetLogBuilder("received error: ").append(cause);

            if (null != explain) {
                logBuilder.append(" (").append(explain).append(')');
            }

            DocumentConverterManager.logError(logBuilder.toString(),
                new LogData("filename", (null != fileName) ? fileName : DocumentConverterUtil.STR_UNKNOWN));
        }
    }

    /**
     * @param outputDir The directory, cotaining all extracted page files
     * @param outputFile The file to contain the single extracred page
     *  or the ZIP archive, containing multiple extracted pages
     * @return The number of pages processed.
     * @throws IOException
     */
    private static int implPostProcess(@NonNull final File outputDir, @NonNull final File outputFile) throws IOException {
        final File[] files = outputDir.listFiles();
        int ret = 0;

        if ((null != files) && ((ret = files.length) > 0)) {

            if (1 == ret) {
                // copy single page file to destination
                FileUtils.copyFile(new File(outputDir, files[0].getName()), outputFile);
            } else {
                // create ZIP archive in case of multiple files
                try (final ZipOutputStream zipOutputStm = new ZipOutputStream(
                        new BufferedOutputStream(
                            FileUtils.openOutputStream(outputFile)))) {

                    for (int i = 0, count = files.length; i < count; ++i) {
                        final String curFilename = files[i].getName();
                        final int realPageNumber = implGetPageInfoFromFilename(curFilename);

                        zipOutputStm.putNextEntry(new ZipEntry(Integer.toString(realPageNumber - 1)));
                        FileUtils.copyFile(new File(outputDir, curFilename), zipOutputStm);
                        zipOutputStm.closeEntry();
                    }

                    zipOutputStm.finish();
                    zipOutputStm.flush();
                }
            }
        } else {
            throw new IOException("PDFExtractor is not able to find any extracted files in directory: " + outputDir);
        }

        return ret;
    }

    /**
     * @param pageFilename The PageCount#PageNumber_PixelWidthxPixelHeight mangled filename
     * @return The page number of the given page string or -1 if not valid
     */
    private static int implGetPageInfoFromFilename(@NonNull String pageFilename) {
        final int hashPos = pageFilename.indexOf('#');
        final int underscorePos = pageFilename.lastIndexOf('_');
        final int crossPos = pageFilename.lastIndexOf('x');

        if ((hashPos > 0) && (underscorePos > (hashPos + 1)) && (crossPos > (underscorePos + 1)) && (crossPos < (pageFilename.length() - 1))) {
            /* possible further values, that can be extracted from filename
            final int originalPageCount = Integer.valueOf(pageFilename.substring(0, hashPos)).intValue();
            final int pixelWidth = Integer.valueOf(pageFilename.substring(underscorePos + 1, crossPos)).intValue();
            final int pixelHeight = Integer.valueOf(pageFilename.substring(crossPos + 1)).intValue();
            */

            return Integer.valueOf(pageFilename.substring(hashPos + 1, underscorePos)).intValue();
        }

        return -1;
    }

    // - Members ---------------------------------------------------------------

    final private DocumentConverterManager m_manager;

    final private File m_pdfExecutable;

    final private String[] m_bashCommandLine;

    final private long m_maxExecTimeMillis;

    // - Static Members --------------------------------------------------------

    private static Logger LOG = LoggerFactory.getLogger(PDFExtractor.class);
 }

/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter;

/**
 * {@link LogData}
 *
 * @author <a href="mailto:malte.timmermann@open-xchange.com">Malte Timmermann</a>
 */
public class LogData {

    /**
     * Initializes a new {@link LogData}.
     *
     * @param _key
     * @param _value
     */
    public LogData(final String _key, final String _value) {
        m_key = _key;
        m_value = _value;
    }

    /**
     * @return
     */
    public String getKey() {
        return ((null != m_key) ? m_key : DocumentConverterUtil.STR_NOT_AVAILABLE);
    }

    /**
     * @return
     */
    public String getValue() {
        return ((null != m_value) ? m_value : DocumentConverterUtil.STR_NOT_AVAILABLE);
    }

    // - Members ---------------------------------------------------------------

    final private String m_key;

    final private String m_value;

}

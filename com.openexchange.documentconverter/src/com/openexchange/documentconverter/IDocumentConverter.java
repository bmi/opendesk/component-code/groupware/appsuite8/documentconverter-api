/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter;

import java.io.InputStream;
import java.util.HashMap;

//------------------
//- IClientManager -
//------------------

/**
 * {@link IDocumentConverter}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 */
public interface IDocumentConverter {

    /**
     * @param jobType The job type fiter to be executed.
     * @param jobProperties Input parameter, containing properties to customize the job (e.g. the InputFile)
     * @param resultProperties Output parameter,  containing additional conversion result properties like e.g. the PageCount of the conversion result.
     * @return The InputStream with the conversion result in case of success or null in case of a failure
     */
    public InputStream convert(String jobType, HashMap<String, Object> jobProperties, HashMap<String, Object> resultProperties);

    /**
     * @param jobType The job type fiter to be executed.
     * @param documentInputStream The InputStream, containing the source document to be converted.
     * @param jobProperties Input parameter, containing properties to customize the job (e.g. the InputFile)
     * @param resultProperties Output parameter,  containing additional conversion result properties like e.g. the error code of the conversion result.
     * @return The job id string to be used in further getConversionPage/endPageConversion jobs or null in case of a failure
     */
    public String beginPageConversion(String jobType, HashMap<String, Object> jobProperties, HashMap<String, Object> resultProperties);

    /**
     * @param jobId The job id gotten from a previous call to beginPageConversion
     * @param pageNumber The number of the page to be returned as input stream.
     * @param jobProperties Input parameter, containing properties to further customize the job, can be empty in this case
     * @param resultProperties Output parameter,  containing additional conversion result properties like e.g. the PageCount of the conversion result.
     * @return The stream the converted page can be read from or null in case of a failure
     */
    public InputStream getConversionPage(String jobId, int pageNumber, HashMap<String, Object> jobProperties, HashMap<String, Object> resultProperties);

    /**
     * @param jobId The job id gotten from a previous call to beginPageConversion
     * @param jobProperties Input parameter, containing properties to further customize the job, can be empty in this case
     * @param resultProperties Output parameter, containing additional conversion result properties like e.g. the error code of the conversion result.
     */
    public void endPageConversion(String jobId, HashMap<String, Object> jobProperties, HashMap<String, Object> resultProperties);
}

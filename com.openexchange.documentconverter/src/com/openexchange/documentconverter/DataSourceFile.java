/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter;

import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

/**
* Compatibility class for deprecated multipart attachments (e.g. 7.6.3).<br>
* <b>!!! Don't change or remove, since it might be used when serializing multipart content !!!</b>
*
* @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
* @since v7.6.0
*/
public class DataSourceFile implements Serializable {

   private static final long serialVersionUID = 1L;

   /**
    * Initializes a new {@link DataSourceFile}.
    */
   public DataSourceFile(@NonNull File sourceFile) {
       super();
       m_sourceFile = sourceFile;
   }

   /**
    * @return The data source file
    */
   public File getSourceFile() {
       return m_sourceFile;
   }

   /**
    * @return The byte array containing the source file content after reading
    */
   public byte[] getDataSinkBuffer() {
       return m_dataSinkBuffer;
   }

   /**
    * @param objInputStm
    * @throws IOException
    * @throws ClassNotFoundException
    */
   private synchronized void readObject(ObjectInputStream objInputStm) throws IOException, ClassNotFoundException {
       m_sourceFile = null;
       m_dataSinkBuffer = null;

       if (null != objInputStm) {
           Object readObj = null;

           objInputStm.defaultReadObject();
           readObj = objInputStm.readObject();

           if (readObj instanceof Integer) {
               final int size = ((Integer) readObj).intValue();

               if (size > 0) {
                   m_dataSinkBuffer = IOUtils.toByteArray(objInputStm);
               }
           }

           m_sourceFile = null;
       }
   }

   /**
    * @param objOutputStm
    * @throws IOException
    */
   private synchronized void writeObject(ObjectOutputStream objOutputStm) throws IOException {
       if (null != objOutputStm) {
           final Integer fileSize = Integer.valueOf((null != m_sourceFile) && (m_sourceFile.canRead()) ? (int) m_sourceFile.length() : 0);

           // write members by default
           objOutputStm.defaultWriteObject();

           // write file size as Long object
           objOutputStm.writeObject(fileSize);

           // write content of data file, if readable and size > 0
           if (fileSize.longValue() > 0) {
               try {
                   FileUtils.copyFile(m_sourceFile, objOutputStm);
               } catch (IOException e) {
                   DocumentConverterManager.logExcp(e);
               }
           }
       }
   }

   // - Members ---------------------------------------------------------------

   private File m_sourceFile;

   private transient byte[] m_dataSinkBuffer = null;
}

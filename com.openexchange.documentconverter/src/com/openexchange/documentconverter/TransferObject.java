/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * {@link TransferObject}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v7.6.0
 */
public class TransferObject <T extends Serializable> {

    /**
     * Initializes a new {@link TransferObject}.
     */
    public TransferObject() {
        super();
    }

    /**
     * Initializes a new {@link TransferObject}.
     */
    public TransferObject(String query, String cookie) {
        super();

        if ((null != query) && (query.length() > 1)) {
            m_paramMap = new HashMap<>(1);
            m_paramMap.put(QUERY_KEY, query);
        }

        m_cookie = cookie;
    }

    /**
     * Initializes a new {@link TransferObject}.
     */
    public TransferObject(T dataObject, String cookie) {
        super();
        m_serialObject = dataObject;
        m_cookie = cookie;
    }

    /**
     * Initializes a new {@link TransferObject}.
     */
    public TransferObject(Map<String, String> paramMap, String cookie) {
        super();
        m_paramMap = paramMap;
        m_cookie = cookie;
    }

    /**
     * Initializes a new {@link TransferObject}.
     * @param dataObject
     * @param filename
     * @param mimeType
     */
    public TransferObject(T dataObject, String filename, String mimeType, String cookie) {
        super();
        m_serialObject = dataObject;
        m_filename = filename;
        m_mimeType = mimeType;
        m_cookie = cookie;
    }

    /**
     * Initializes a new {@link TransferObject}.
     * @param paramMap
     * @param dataObject
     * @param filename
     * @param mimeType
     */
    /**
     * Initializes a new {@link TransferObject}.
     * @param query
     * @param dataObject
     * @param filename
     * @param mimeType
     */
    public TransferObject(String query, T dataObject, String filename, String mimeType, String cookie) {
        this(dataObject, filename, mimeType, cookie);

        if ((null != query) && (query.length() > 1)) {
            m_paramMap = new HashMap<>(1);
            m_paramMap.put(QUERY_KEY, query);
        }
    }

    /**
     * Initializes a new {@link TransferObject}.
     * @param paramMap
     * @param dataObject
     * @param filename
     * @param mimeType
     */
    public TransferObject(Map<String, String> paramMap, T dataObject, String filename, String mimeType, String cookie) {
        this(dataObject, filename, mimeType, cookie);
        m_paramMap = paramMap;
    }

    /**
     *
     */
    public void clear() {
        m_paramMap = null;
        m_serialObject = null;
        m_filename = null;
        m_mimeType = null;
        m_cookie = null;
    }

    /**
     * @return  The query, built from the given query string and all
     *          other key/value pairs within the parameter map
     */
    public String getQuery() {
        String ret = null;

        if (null != m_paramMap) {
            // add the query string first, if given
            ret = m_paramMap.get(QUERY_KEY);

            // add the other key/value pairs to the query string
            if ((m_paramMap.size() > 1) || ((1 == m_paramMap.size()) && (null == ret))) {
                final StringBuilder queryBuilder = new StringBuilder((null != ret) ? ret : "");

                for (String curKey : m_paramMap.keySet()) {
                    if ((null != curKey) && !curKey.equals(QUERY_KEY)) {
                        if (queryBuilder.length() > 0) {
                            queryBuilder.append('&');
                        }

                        queryBuilder.append(curKey);
                        queryBuilder.append('=');
                        queryBuilder.append(m_paramMap.get(curKey));
                    }
                }

                // get the final query string
                ret = queryBuilder.toString();
            }
        }

        return ret;
    }

    /**
     * @param query
     */
    public void setQuery(String query) {
        if ((null != query) && (query.length() > 1)) {
            if (null == m_paramMap) {
                m_paramMap = new HashMap<>(1);
            }

            m_paramMap.put(QUERY_KEY, query);
        } else if (null != m_paramMap) {
            m_paramMap.remove(QUERY_KEY);
        }
    }

    /**
     * @return
     */
    public Map<String, String> getParamMap() {
        return m_paramMap;
    }

    /**
     * @param paramMap
     */
    public void setParamMap(Map<String, String> paramMap) {
        this.m_paramMap = paramMap;
    }

    /**
     * @return
     */
    public T getSerialObject() {
        return m_serialObject;
    }

    /**
     * @param serialObject
     */
    @SuppressWarnings("unchecked")
    public void setSerialObject(Object serialObject) {
        this.m_serialObject = (T) serialObject;
    }

    /**
     * @return
     */
    public String getFilename() {
        return m_filename;
    }

    /**
     * @param filename
     */
    public void setFilename(String filename) {
        this.m_filename = filename;
    }

    /**
     * @return
     */
    public String getMimeType() {
        return m_mimeType;
    }

    /**
     * @param mimeType
     */
    public void setMimeType(String mimeType) {
        this.m_mimeType = mimeType;
    }

    /**
     * @return
     */
    public String getCookie() {
        return m_cookie;
    }

    /**
     * @param cookie
     */
    public void setCookie(String cookie) {
        this.m_cookie = cookie;
    }

    // - Members ---------------------------------------------------------------

    final protected static String QUERY_KEY = "query";

    // -------------------------------------------------------------------------

    private Map<String, String> m_paramMap = null;
    private T m_serialObject = null;
    private String m_filename = null;
    private String m_mimeType = null;
    private String m_cookie = null;
}

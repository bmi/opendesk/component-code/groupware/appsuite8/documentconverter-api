Name:          open-xchange-documentconverter-client
BuildArch:     noarch
BuildRequires: java-1.8.0-openjdk-devel
BuildRequires: ant
BuildRequires: open-xchange-documentconverter-api >= @OXVERSION@
Version:       @OXVERSION@
%define        ox_release 3
Release:       %{ox_release}_<CI_CNT>.<B_CNT>
Group:         Applications/Productivity
License:       AGPLv3+
BuildRoot:     %{_tmppath}/%{name}-%{version}-build
URL:           http://www.open-xchange.com/
Source:        %{name}_%{version}.orig.tar.bz2
Summary:       The Open-Xchange backend client for office document conversion
AutoReqProv:   no
Requires:      open-xchange-documentconverter-api >= @OXVERSION@
Requires:      open-xchange-pdftool >= 1.0
Provides:      open-xchange-documentconverter = %{version}
Obsoletes:     open-xchange-documentconverter <= %{version}

%description
This package contains the backend components for the documentconverter client

Authors:
--------
    Open-Xchange

%prep

%setup -q

%build

%install
export NO_BRP_CHECK_BYTECODE_VERSION=true
ant -lib build/lib -Dbasedir=build -DdestDir=%{buildroot} -DpackageName=%{name} -f build/build.xml clean build

%post
if [ ${1:-0} -eq 2 ]; then
  # only when updating
  . /opt/open-xchange/lib/oxfunctions.sh

  # prevent bash from expanding, see bug 13316
  GLOBIGNORE='*'

  # SCR-234
  # SoftwareChange_Request-234
  pfile=/opt/open-xchange/etc/documentconverter-client.properties
  inst_dir_k=com.openexchange.documentconverter.client.pdfextractor.installDir
  inst_dir_v=/opt/open-xchange/sbin
  work_dir_k=com.openexchange.documentconverter.client.pdfextractor.workDir
  work_dir_v=/tmp
  ox_add_property ${inst_dir_k} ${inst_dir_v} ${pfile}
  ox_add_property ${work_dir_k} ${work_dir_v} ${pfile}

  # SCR-544
  if ox_scr_todo 544
  then
    pfile=/opt/open-xchange/etc/documentconverter-client.properties
    work_dir_k=com.openexchange.documentconverter.client.pdfextractor.workDir
    value=$(ox_read_property ${work_dir_k} ${pfile})
    if [ "/tmp" = "${value//[[:space:]]/}" ]
    then
      ox_set_property ${work_dir_k} "" ${pfile}
    fi
    ox_scr_done 544
  fi

  if ox_scr_todo SCR-708
  then
    ox_add_property com.openexchange.documentconverter.client.pdfextractor.maxVMemMB 1024 /opt/open-xchange/etc/documentconverter-client.properties
    ox_scr_done SCR-708
  fi
fi

%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-,root,root)
%dir /opt/open-xchange/bundles/
/opt/open-xchange/bundles/*
%dir /opt/open-xchange/osgi/bundle.d/
/opt/open-xchange/osgi/bundle.d/*
%dir /opt/open-xchange/etc/
%config(noreplace) /opt/open-xchange/etc/documentconverter-client.properties
/opt/open-xchange/etc/security/documentconverter-client.list

%changelog
* Thu Nov 28 2019 Marcus Klein <marcus.klein@open-xchange.com>
Second candidate for 7.10.3 release
* Thu Nov 21 2019 Marcus Klein <marcus.klein@open-xchange.com>
First candidate for 7.10.3 release
* Thu Oct 17 2019 Marcus Klein <marcus.klein@open-xchange.com>
First preview for 7.10.3 release
* Tue Jun 18 2019 Marcus Klein <marcus.klein@open-xchange.com>
prepare for 7.10.3 release
* Thu May 02 2019 Marcus Klein <marcus.klein@open-xchange.com>
Second preview of 7.10.2 release
* Thu Mar 28 2019 Marcus Klein <marcus.klein@open-xchange.com>
First preview of 7.10.2 release
* Mon Mar 11 2019 Marcus Klein <marcus.klein@open-xchange.com>
prepare for 7.10.2
* Fri Nov 23 2018 Marcus Klein <marcus.klein@open-xchange.com>
RC 1 for 7.10.1 release
* Fri Nov 02 2018 Marcus Klein <marcus.klein@open-xchange.com>
Second preview for 7.10.1 release
* Thu Oct 11 2018 Marcus Klein <marcus.klein@open-xchange.com>
First candidate for 7.10.1 release
* Mon Sep 10 2018 Marcus Klein <marcus.klein@open-xchange.com>
prepare for 7.10.1
* Mon Jun 11 2018 Marcus Klein <marcus.klein@open-xchange.com>
First candidate for 7.10.0 release
* Fri May 18 2018 Marcus Klein <marcus.klein@open-xchange.com>
Sixth preview of 7.10.0 release
* Thu Apr 19 2018 Marcus Klein <marcus.klein@open-xchange.com>
Fifth preview of 7.10.0 release
* Tue Apr 03 2018 Marcus Klein <marcus.klein@open-xchange.com>
Fourth preview of 7.10.0 release
* Tue Feb 20 2018 Marcus Klein <marcus.klein@open-xchange.com>
Third preview of 7.10.0 release
* Fri Feb 02 2018 Marcus Klein <marcus.klein@open-xchange.com>
Second preview for 7.10.0 release
* Fri Dec 01 2017 Marcus Klein <marcus.klein@open-xchange.com>
First preview for 7.10.0 release
* Mon Oct 16 2017 Marcus Klein <marcus.klein@open-xchange.com>
prepare for 7.10.0 release
* Tue May 16 2017 Marcus Klein <marcus.klein@open-xchange.com>
First candidate for 7.8.4 release
* Thu May 04 2017 Marcus Klein <marcus.klein@open-xchange.com>
Second preview of 7.8.4 release
* Mon Apr 03 2017 Marcus Klein <marcus.klein@open-xchange.com>
First preview of 7.8.4 release
* Fri Dec 02 2016 Marcus Klein <marcus.klein@open-xchange.com>
prepare for 7.8.4 release
* Fri Nov 25 2016 Marcus Klein <marcus.klein@open-xchange.com>
Second release candidate for 7.8.3 release
* Thu Nov 24 2016 Marcus Klein <marcus.klein@open-xchange.com>
First release candidate for 7.8.3 release
* Tue Nov 15 2016 Marcus Klein <marcus.klein@open-xchange.com>
Third preview for 7.8.3 release
* Sat Oct 29 2016 Marcus Klein <marcus.klein@open-xchange.com>
Second preview for 7.8.3 release
* Fri Oct 14 2016 Marcus Klein <marcus.klein@open-xchange.com>
First preview of 7.8.3 release
* Tue Sep 06 2016 Marcus Klein <marcus.klein@open-xchange.com>
prepare for 7.8.3 release
* Tue Jul 12 2016 Marcus Klein <marcus.klein@open-xchange.com>
Second candidate for 7.8.2 release
* Wed Jul 06 2016 Marcus Klein <marcus.klein@open-xchange.com>
First candidate for 7.8.2 release
* Wed Jun 29 2016 Marcus Klein <marcus.klein@open-xchange.com>
Second preview for 7.8.2 release
* Tue Jun 14 2016 Marcus Klein <marcus.klein@open-xchange.com>
First candidate for 7.8.2 release
* Wed May 11 2016 Marcus Klein <marcus.klein@open-xchange.com>
initial packaging for open-xchange-documentconverter-client
